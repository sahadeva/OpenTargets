import argparse
import sys
import os
import logging
import re
import traceback

from datetime import datetime

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from lib import ot_wrapper as otew
from lib import therapeutic_area_parser as tap
from lib import oboparser as obo

class assocAction(argparse.Action):
    def __call__(self,parser,namespace,values,option_string=None):
        if not 0<=values<=1:
            raise argparse.ArgumentError(self,'association score threshold must be between 0 and 1, but found {}'.format(values))
        setattr(namespace,self.dest,values)

class retmaxAction(argparse.Action):
    def __call__(self,parser,namespace,values,option_string=None):
        if not 1<=values<=10000:
            raise argparse.ArgumentError(self,'retmax value must be between 1 and 10,000, but found {}'.format(values))
        setattr(namespace,self.dest,values)

def check_efo_term(efo,term,subps):
    '''
    raise error is one of the arugment is given and other is None
    '''
    if (efo is None and term is not None) or (efo is not None and term is None):
        subps.print_help()
        raise argparse.ArgumentTypeError("Found --efo {} and --term {}. Need valid arguments for '--efo' and '--term' or both must be none!".format(efo,term))
    elif (efo is None) and (term is None):
        return False
    else:
        return True

def check_efo(efo,subps):
    '''
    raise error if efo is not given
    '''
    if efo is None:
        subps.print_help()
        raise argparse.ArgumentTypeError('error: the following arguments are required: --efo')

def id_reformatter(ids):
    '''
    ids in obo file have the format EFO:\d+ and 
    ids in opentargets evidence dump have the format EFO_\d+
    change ":" to "_"
    '''
    return set([i.replace(':','_') for i in ids])

def main(argv):
    prog = re.sub(r'^.*'+os.sep,'',argv[0])
    description = '''
    parse variant data from OpenTargets (https://www.targetvalidation.org/) evidence objects dump (https://www.targetvalidation.org/downloads/data)
    This workflow is designed to work with evidence schema from : https://storage.googleapis.com/open-targets-data-releases/20.02/output/20.02_evidence_data.json.gz

    Positional arguments:

        variant
        ========
        Parse variants for the association types 'somatic_mutation' and 'genetic_associations'
        For variant ids clinvar or dbsnp databases, fetch chromosomal locations and map these variants
        to disease - genes if the variant chromosomal location is within that of gene location
        Output:
            * bed formatted variant location data, with name in the following format: variantId|geneId|geneName|diseaseId|diseaseName|associationType
        
        disease_variant
        ===============
        Parse [disease ids] from input Experimental Factor Ontology (EFO), and for variant ids clinvar or dbsnp databases, fetch chromosomal locations and map these variants
        to disease - genes if the variant chromosomal location is within that of gene location.
        [disease ids] : child nodes of the term "disease" in EFO.
        Output:
            * bed formatted variant location data, with name in the following format: variantId|geneId|geneName|diseaseId|diseaseName|associationType
        
        multiterm_variant
        =================
        Parse child nodes of given terms from EFO and for variants mapping to these ids, fetch chromosomal locations and map these variants
        to disease - genes if the variant chromosomal location is within that of gene location.
        [ids] : child nodes of input terms in EFO.
        Output:
            * bed formatted variant location data, with name in the following format: variantId|geneId|geneName|diseaseId|diseaseName|associationType

        obo_id_name
        ============
        Parse the given .obo file and write id and name annotation of the elements as a <TAB> separted file.

        therapeutic_area
        ================
        Parse therapeutic area annotations for each diease and output these annotations in <TAB> separated format
        If EFO ontology in obo format and a term or id is given, thereapeutic area annotations are filtered for descendants of the term
        Input --term can either be an id in the ontology or an exact name as it is given in the ontology.

    '''
    # OpenTargets arguments
    otargs = argparse.ArgumentParser(add_help=False)
    otargs.add_argument('--in',metavar='input file',dest='otDump',help='input file name for OpenTargets evidence dump (supports .gz files)',required=True)
    # minscore args
    mnargs = argparse.ArgumentParser(add_help=False)
    mnargs.add_argument('--associationScore',metavar='association score',dest='assocScore',help='Fitler out evidences with association score < associationScore (default: 0.20)',type=float,default=0.20,action=assocAction)
    # Variant arguments
    variantTypes = ['somatic_mutation','genetic_association']
    varargs = argparse.ArgumentParser(add_help=False)
    varargs.add_argument('--type',metavar='association type',dest='assocTypes',help='variant association evidence type. Allowed choices {} (default: somatic_muation)'.format(', '.join(variantTypes)),choices=variantTypes,default='somatic_mutation')
    # mendelian arguments
    mimArgs = argparse.ArgumentParser(add_help=False)
    mimArgs.add_argument('--mendelian',dest='mendelian',help='Flag to restrict association types to Mendelian disease association from databases: UniProt, Genomics England PanelApp, UniProt literature, EVA and Gene2Phenotype',action='store_true')
    # efetch arguments
    efargs = argparse.ArgumentParser(add_help=False)
    efargs.add_argument('--email',metavar='E-mail',dest='email',help='valid email address for Entrez eutils (see: https://www.ncbi.nlm.nih.gov/books/NBK25499/)',required=True)
    efargs.add_argument('--retmax',metavar='retmax',dest='retmax',help='Maximum number of ids to retrieve at a time (see: https://www.ncbi.nlm.nih.gov/books/NBK25499/ default: 2500)',type=int,default=2500,action=retmaxAction)
    # clinvar arguments
    cvargs = argparse.ArgumentParser(add_help=False)
    cvargs.add_argument('--measureSet',metavar='measure set',dest='measureSet',help='clinvar measureset parameter to use (see: https://www.ncbi.nlm.nih.gov/clinvar/docs/ftp_primer/ default: Variant)',type=str,default='Variant')
    cvargs.add_argument('--assembly',metavar='assembly',dest='assembly',help='Genome assembly version to parse co-ordinated from (default: GRCh38)',type=str,default='GRCh38')
    #gff args
    gffargs = argparse.ArgumentParser(add_help=False)
    gffargs.add_argument('--gff',metavar='annotation',dest='gff',help='Genome annotation file in gff3 format (MUST be from ENSEMBL, supports .gz files)',required=True)
    # obo args
    oboargs = argparse.ArgumentParser(add_help=False)
    oboargs.add_argument('--efo',metavar='experimental factor ontology (EFO)',dest='efo',help='Experimental factor ontology in .obo format (https://www.ebi.ac.uk/efo/efo.obo)',type=str,default=None)
    # term args
    termargs = argparse.ArgumentParser(add_help=False)
    termargs.add_argument('--term',metavar='EFO term',dest='term',help='parse only the child nodes of this term for attributes',type=str,default=None)
    # multi term args
    multtermargs = argparse.ArgumentParser(add_help=False)
    multtermargs.add_argument('--terms',metavar='EFO term(s)',dest='terms',help='parse only the child nodes of the given terms. Input: space separated list of terms, (for terms with space in names, enclose in "") ',type=str,required=True,nargs='+')
    # out args
    outargs = argparse.ArgumentParser(add_help=False)
    outargs.add_argument('--out',metavar='output file name',dest='out',help='File name to write output bed formatted data',required=True)
    # log arguments
    loglevels = ['debug','info','warning','error','quiet']
    largs = argparse.ArgumentParser(add_help=False)
    largs.add_argument('--verbose',metavar='Verbose level',dest='verbose',help='Allowed choices: {} (default: info)'.format(', '.join(loglevels)),choices=loglevels,default='info')
    # main args
    epilog = "For command line options of each argument, use: {} <positional argument> -h".format(prog)
    oteargs = argparse.ArgumentParser(prog=prog,description=description,formatter_class=argparse.RawDescriptionHelpFormatter,epilog=epilog)
    subps = oteargs.add_subparsers(help='Need positional arguments',dest='subparser')
    # variant parser
    vpdes = 'parse variant data for the given associations types. For clinvar and dbsnp variants, fetch positional information from Entrez utils and output in bed format'
    vparser = subps.add_parser('variant',help='parse variants',description=vpdes,parents=[otargs,mnargs,varargs,mimArgs,efargs,cvargs,gffargs,outargs,largs])
    # disease variant parser
    dvpdes = 'parse variant data for disease ids from EFO. For clinvar and dbsnp variants, fetch positional information from Entrez utils and output in bed format'
    dvparser = subps.add_parser('disease_variant',help='parse disease variants',description=dvpdes,parents=[oboargs,otargs,mnargs,efargs,cvargs,gffargs,outargs,largs])
    # multi term variant parser
    mtvdes = 'parse variant data for child nodes of input terms from EFO. For clinvar and dbsnp variants, fetch positional information from Entrez utils and output in bed format '
    mtvparser = subps.add_parser('multiterm_variant',help='parse variants for multiple terms',description=mtvdes,parents=[oboargs,multtermargs,otargs,mnargs,efargs,cvargs,gffargs,outargs,largs])
    # therapeutic area parser
    tades = 'parse disease to thereapeutic area annotations from the given dump and write the mappings as <TAB> separated file. If EFO ontology in obo format and a term or id is given, thereapeutic area annotations are filtered for descendants of the term'
    taparser = subps.add_parser('therapeutic_area',help='parse therapeutic area annotations',description=tades,parents=[otargs,oboargs,termargs,outargs,largs])
    # obo id to name
    obodes = 'parse .obo file and create id to name mapping file'
    oboparser = subps.add_parser('obo_id_name',help='obo id to name',description=obodes,parents=[oboargs,outargs,largs])
    try:
        otvars = oteargs.parse_args()
        otsub = otvars.subparser
        if otsub is None:
            oteargs.print_help()
            sys.exit(1)
        if otvars.verbose == 'quiet':
            pass
        else:
            logger = logging.getLogger()
            logger.setLevel(logging.getLevelName(otvars.verbose.upper()))
            if len(logger.handlers)>=1:
                logger.handlers = []
            consHandle = logging.StreamHandler(sys.stderr)
            consHandle.setLevel(logging.getLevelName(otvars.verbose.upper()))
            consHandle.setFormatter(logging.Formatter(' [%(levelname)s] %(message)s'))
            logger.addHandler(consHandle)
        logging.info('Starting run @ {}'.format(datetime.now().strftime('%Y-%m-%d %H:%M')))
        if otsub == 'variant':
            dbs = None
            if otvars.mendelian:
                dbs = ['uniprot','EVA','Genomics England PanelApp','Gene2Phenotype','Uniprot literature']
            vp = otew.VariantAnnotator(otDump=otvars.otDump,minScore=otvars.assocScore,email=otvars.email,
                            retmax=otvars.retmax,clinvarMeasureSet=otvars.measureSet,clinvarAssembly=otvars.assembly)
            vp.getVariants(assoTypes=[otvars.assocTypes],databases=dbs,gff=otvars.gff,out=otvars.out)
        elif otsub == 'disease_variant':
            check_efo(otvars.efo,dvparser)
            efop = obo.EfoParser(otvars.efo)
            diseaseIds = id_reformatter(efop.getDescendants('disease'))
            dvp = otew.VariantAnnotator(otDump=otvars.otDump,minScore=otvars.assocScore,email=otvars.email,
                            retmax=otvars.retmax,clinvarMeasureSet=otvars.measureSet,clinvarAssembly=otvars.assembly)
            dvp.getDiseaseVariants(diseaseIds=diseaseIds,gff=otvars.gff,out=otvars.out)
        elif otsub == 'multiterm_variant':
            inputIds = set()
            multTerm = otvars.terms
            efop = obo.EfoParser(otvars.efo)
            for mt in multTerm:
                try:
                    inputIds.update(efop.getDescendants(mt))
                except LookupError as le:
                    logging.error(str(le)+', skipping')
                    pass
            if len(inputIds)==0:
                raise LookupError('Cannot find child nodes in {} for given terms {}. Check your input file and terms!'.format(otvars.efo,', '.join(multTerm)))
            logging.info('Parsed {} ids from {}'.format(len(inputIds),otvars.efo))
            inputIds = id_reformatter(inputIds)
            mvp = otew.VariantAnnotator(otDump=otvars.otDump,minScore=otvars.assocScore,email=otvars.email,
                            retmax=otvars.retmax,clinvarMeasureSet=otvars.measureSet,clinvarAssembly=otvars.assembly)
            mvp.getDiseaseVariants(diseaseIds=inputIds,gff=otvars.gff,out=otvars.out)
        elif otsub == 'therapeutic_area':
            validInputs = check_efo_term(otvars.efo,otvars.term,taparser)
            if validInputs:
                efop = obo.EfoParser(otvars.efo)
                ids = id_reformatter(efop.getDescendants(otvars.term))
                with tap.TherapeuticArea(otvars.otDump) as tapt:
                    tapt.parser(outfile=otvars.out,filterIds=ids)
            else:
                with tap.TherapeuticArea(otvars.otDump) as tapt:
                    tapt.parser(outfile=otvars.out)
        elif otsub == 'obo_id_name':
            check_efo(otvars.efo,oboparser)
            efop = obo.EfoParser(otvars.efo)
            efop.mapIdToName(otvars.out)

        logging.info('Run finished @ {}'.format(datetime.now().strftime('%Y-%m-%d %H:%M')))
    except KeyboardInterrupt:
        sys.stderr.write('Keyboard interrupt....Good bye\n')
        sys.exit(1)
    except Exception:
        traceback.print_exc(file=sys.stdout)
    sys.exit(0)

if __name__ == '__main__':
    main(sys.argv)