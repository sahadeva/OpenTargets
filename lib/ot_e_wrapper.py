import logging
import time
from lib.variantparser import VariantParser
from lib.eparsers import clinVarParser, snpParser
from lib.ensemblparser import EnsemblRequest
from lib.gffparser import GffParser

class VariantAnnotator:

    def __init__(self,otDump,assoTypes,minScore,email,retmax,clinvarMeasureSet,clinvarAssembly):
        self.otDump = otDump
        self.assoTypes = assoTypes
        self.minScore = minScore
        self.email = email
        self.retmax = retmax
        self.clinvarMeasureSet = clinvarMeasureSet
        self.clinvarAssembly = clinvarAssembly
    
    def _parseOpenTargets(self,databases=None):
        with VariantParser(otDump=self.otDump,assocTypes=self.assoTypes,minScore=self.minScore,databases=databases) as vp:
            vp.parser()
            self._geneNameMap = vp.geneIdName
            self._diseaseNameMap = vp.diseaseIdName
            clinvarIds = vp.clinvarIds
            dbSnpIds = vp.dbSnpIds
            noDbSnpIds = vp.noDbSnpIds
            self._disVarMap = vp.diseaseGeneVariance
            self._diseaseGeneAssoc = vp.diseaseGeneAssociation
        self._sanityCheck(self._geneNameMap,'gene id to name mappings')
        self._sanityCheck(self._diseaseNameMap,'disease id to name mappings')
        self._sanityCheck(self._disVarMap,'disease gene variance data')
        if len(dbSnpIds)==0:
            logging.warning('Cannot find dbSNP ids! Check your dataset and input parameters')
            dbSnpMap = {}
        else:
            snpparser = snpParser(dbSnpIds,self.email,self.retmax)
            snpparser.efetch()
            dbSnpMap = snpparser.mapping
        if len(clinvarIds)==0:
            logging.warning('Cannot find clinVar ids! Check your dataset and input parameters')
            clinvarMap = {}
        else:
            time.sleep(20)
            # wait for 20 sec before fetching clinvar data
            # to not to overload Entrez servers
            clinparser = clinVarParser(clinvarIds,self.email,self.retmax,self.clinvarMeasureSet,self.clinvarAssembly)
            clinparser.efetch()
            clinvarMap = clinparser.mapping
        if len(noDbSnpIds)==0:
            logging.warning('Cannot find no dbSNP data!')
            noDbSnpMap = {}
        else:
            noDbSnpMap = {}
            for nids in noDbSnpIds:
                chrom,pos,reflen = nids.split('_')
                begin = int(pos)-1
                end = begin+int(reflen)
                noDbSnpMap[nids] = [[chrom,begin,end,'*']]
        if len(dbSnpMap)==0 and len(clinvarMap)==0:
            raise ValueError('Cannot parse chromosomal locations for dbSNP ids or clinvar ids!')
        self._idLocMap = {**dbSnpMap,**clinvarMap,**noDbSnpMap}
    
    def _sanityCheck(self,dat,name):
        '''
        heler function
        check if given dat is empty and throw an exception
        '''
        if len(dat)==0:
            raise ValueError('Cannot parse {} from {}. Check your dataset and input parameters!'.format(name,self.otDump))
    
    def _getGeneLocations(self,gff):
        with GffParser(gff=gff) as _gf:
            _gf.parseGff()
            self._geneLocMap = _gf.getGenePosition()
        missingGenes =  set(self._geneNameMap.keys()) - set(self._geneLocMap.keys())
        if len(missingGenes)>0:
            logging.info('Cannot find chromosomal locations for {} genes, fetching data from ENSEMBL'.format(len(missingGenes)))
            ensparser = EnsemblRequest(missingGenes)
            for ids, dat in ensparser.getPosition().items():
                self._geneLocMap[ids] = dat
        else:
            logging.info('Found positional data for {} genes from file {}'.format(len(self._geneNameMap),gff))

    def _writeData(self,out):
        rejectedIdMap = {} # ids rejected previously for positional inconsistencies
        with open(out,'w') as oH:
            for disease in self._disVarMap:
                commonGenes = set(self._disVarMap[disease].keys()) & set(self._geneLocMap.keys())
                if len(commonGenes)==0:
                    logging.debug('{}'.format(','.join(self._disVarMap[disease].keys())))
                    logging.warning('Cannot find any genes with chromosome position for {}, skipping...'.format(disease))
                    continue
                diseaseName = self._diseaseNameMap[disease] if disease in self._diseaseNameMap else 'unknown'
                for gene in commonGenes:
                    commonIds = set(self._disVarMap[disease][gene].keys()) & set(self._idLocMap.keys())
                    if gene in rejectedIdMap:
                        commonIds = commonIds - rejectedIdMap[gene]
                    if len(commonIds)==0:
                        logging.debug('{}'.format(','.join(self._disVarMap[disease][gene].keys())))
                        logging.warning('Cannot find chromosome location of variants in disease {}, gene {}, skipping'.format(disease,gene))
                        continue
                    geneLoc = self._geneLocMap[gene]
                    geneName = self._geneNameMap[gene] if gene in self._geneNameMap else 'undefined'
                    rejIds = set()
                    for ids in commonIds:
                        assoScore = self._disVarMap[disease][gene][ids]
                        try:
                            assoSet = ','.join(self._diseaseGeneAssoc[disease][gene][ids])
                        except KeyError:
                            assoSet = 'None'
                        for varLoc in self._idLocMap[ids]:
                            if (varLoc[0]!=geneLoc[0]) or varLoc[1]<geneLoc[1] or varLoc[2]>geneLoc[2]:
                                logging.info('variant {} {}:{}-{} incompatible location for gene {} {}:{}-{} for {}, skipping'.format(ids,varLoc[0],varLoc[1],varLoc[2],gene,geneLoc[0],geneLoc[1],geneLoc[2],disease))
                                rejIds.add(ids)
                                continue
                            varLen = varLoc[2]-varLoc[1]
                            geneLen = geneLoc[2] - geneLoc[1]
                            if varLen>=geneLen:
                                logging.info('variant {} {}:{}-{} and gene {} {}:{}-{} for {} have same length! skipping'.format(ids,varLoc[0],varLoc[1],varLoc[2],gene,geneLoc[0],geneLoc[1],geneLoc[2],disease))
                                rejIds.add(ids)
                                continue
                    
                            varName = '|'.join([ids,gene,geneName,disease,diseaseName,assoSet])
                            oH.write('\t'.join([ varLoc[0],str(varLoc[1]),str(varLoc[2]),varName,str(assoScore),varLoc[3]])+'\n')
                    try:
                        rejectedIdMap[gene].update(rejIds)
                    except KeyError:
                        rejectedIdMap[gene] = rejIds
    
    def getVariantData(self,out,gff,databases=None):
        '''
        wrapper function
        '''
        logging.info('Parsing {} for {} variants, minimum association score required: {}'.format(self.otDump,','.join(self.assoTypes),self.minScore))
        self._parseOpenTargets(databases=databases)
        self._getGeneLocations(gff=gff)
        logging.info('Output file: {}'.format(out))
        self._writeData(out=out)






    
        