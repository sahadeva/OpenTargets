import gzip
import json
import re
import logging

from collections import defaultdict

class OpenTargetsParser:

    def __init__(self,otDump,minScore=0.2):
        self.otDump = otDump
        self.minScore = 0.2
        logging.debug(self.otDump)
    
    def __enter__(self):
        if self._is_gzip():
            logging.debug('{} is gzipped'.format(self.otDump))
            self._otH = gzip.open(self.otDump,'rt')
        else:
            logging.debug('{} is flat'.format(self.otDump))
            self._otH = open(self.otDump,'r')
        self._lowScore = 0
        self._geneId2Name = {}
        self._diseaseId2Name = {}
        return self
    
    def __exit__(self,exceptType,exceptVal,exceptTback):
        self._otH.close()
        if exceptType:
            logging.exception(exceptVal)
    
    def _is_gzip(self):
        if re.match(r'^.*\.gz$',self.otDump,re.IGNORECASE):
            return True
        else:
            return False
    
    def _setDiseaseIdName(self,jsonDict):
        '''
        Helper function
        populate disease Id to name dictionary and return disease id
        '''
        disName = None
        disId = None
        try:
            disName = jsonDict['efo_info']['label']
        except KeyError:
            pass
        try:
            disId = jsonDict['id']
        except KeyError:
            pass
        if (disId is not None) and (disName is not None):
            self._diseaseId2Name[disId] = disName
    
    def _setGeneIdName(self,jsonDict):
        '''
        Helper function
        populate gene Id to name dictionary and return gene id
        '''
        geneName = None
        geneId = None
        try:
            geneName = jsonDict['gene_info']['symbol']
        except KeyError:
            pass
        try:
            geneId = jsonDict['id']
        except KeyError:
            pass
        if (geneId is not None) and (geneName is not None):
            self._geneId2Name[geneId] = geneName
    
    @property
    def diseaseIdName(self):
        return self._diseaseId2Name
    
    @property
    def geneIdName(self):
        return self._geneId2Name



