'''
This module contains custom exceptions
'''

class ParserError(LookupError):
    '''
    if something is not found in mapping data
    '''
    pass

class OboUndirectedNetworkError(RuntimeError):
    '''
    Input Obo file is parsed as undirected network
    '''
    pass

class NoDecendantsError(LookupError):
    '''
    if given node has no child nodes
    '''
    pass

class NoVariantMappingError(ValueError):
    '''
    if there are no snp id to chromosomal location mappings
    '''