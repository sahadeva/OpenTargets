import logging
import math
import requests
import time
import json

class EnsemblRequest:
    '''
    retrieve chromosome locations for a given set of ensemble gene ids
    '''
    def __init__(self,ids,maxtries=5):
        '''
        source: https://rest.ensembl.org/documentation/info/lookup_post
        '''
        self.ids = list(ids)
        self.maxtries = 5
        self.maxquery = 500
        self.lookup = 'https://rest.ensembl.org/lookup/id'
        self.headers = { "Content-Type" : "application/json", "Accept" : "application/json"}
        self.wait = 15
        self._mapping = {}
    
    def getPosition(self):
        counter = int(math.ceil(len(self.ids)/self.maxquery))
        retstart = 0
        retmax = min(len(self.ids),self.maxquery)
        for i in range(counter):
            logging.info('chunk {} ids {} of {}'.format(i+1,retmax,len(self.ids)))
            self._request(self.ids[retstart:retmax])
            retstart = retmax
            retmax = min(retmax+self.maxquery,len(self.ids))
            time.sleep(self.wait)
        diffIds = set(self.ids) - set(self._mapping.keys())
        if len(diffIds)>0:
            logging.warning('Missing gene location info for {} genes!'.format(len(diffIds)))
            logging.warning('Missing ids : {}'.format(', '.join(diffIds)))
        return self._mapping
    
    def _request(self,ids):
        '''
        rq = requests.post(url, headers=headers, data = json.dumps({'ids':testIds}))
        '''
        tryCount = 0
        while tryCount < self.maxtries:
            rqResponse = requests.post(self.lookup,headers=self.headers,data=json.dumps({'ids':ids}))
            if 500 <= rqResponse.status_code <= 504:
                logging.warning('ENSEMBL server seems to be busy, try {} of {}'.format(tryCount+1,self.maxtries))
                time.sleep(30)
                tryCount+=1
            else:
                break
        if tryCount>=self.maxtries:
            raise ConnectionError('ENSEMBL server fail to respond! Check {} server status manually'.format(rqResponse.url))
        elif rqResponse.ok:
            logging.debug('Found mapping for {} gene ids'.format(len(rqResponse.json())))
            for ids, dat in rqResponse.json().items():
                if dat is None:
                    logging.warning('Cannot find chromosomal locations for id: {}, skipping'.format(ids))
                    continue
                if ('start' not in dat) or ('end' not in dat) or ('seq_region_name' not in dat):
                    logging.warning('Cannot find chromosomal co-ordinates for {}, skipping'.format(ids))
                    continue
                try:
                    strand = '+' if dat['strand'] == 1 else '-'
                except KeyError:
                    strand = '*'
                chrom = dat['seq_region_name']
                if chrom == 'MT':
                    chrom = 'M'
                self._mapping[ids] = [ 'chr'+chrom,dat['start']-1,dat['end'],strand]
        else:
            rqResponse.raise_for_status()


    



