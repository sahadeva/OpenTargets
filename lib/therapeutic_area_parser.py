import gzip
import json
import re
import logging
import os

from lib.otparser import OpenTargetsParser
from lib.exceptions import ParserError

class TherapeuticArea(OpenTargetsParser):
    
    def __init__(self, otDump, minScore=0.0):
        super().__init__(otDump, minScore=minScore)
    
    def parser(self,outfile,filterIds=None):
        taMap = {}
        idNotPresent = self._getIdChecker(filterIds)
        i=0
        for ol in self._otH:
            otJson = json.loads(ol)
            i+=1
            if (i%100000)==0:
                logging.info('Parsed {} lines'.format(i))
            if not 'disease' in otJson: 
                continue
            if idNotPresent(otJson,filterIds):
                continue
            if not 'efo_info' in otJson['disease']:
                continue
            if not 'therapeutic_area' in otJson['disease']['efo_info']:
                continue
            for c in otJson['disease']['efo_info']['therapeutic_area']['codes']:
                try:
                    taMap[c].add(otJson['disease']['id'])
                except KeyError:
                    taMap[c] = set([otJson['disease']['id']])
        logging.info('Parsed {} lines'.format(i))
        if len(taMap)==0:
            raise ParserError('Cannot find therapeutic area annotations in {}'.format(self.otDump))
        logging.info('Found annotations for {} therapeutic areas'.format(len(taMap)))
        if os.path.exists(outfile):
            logging.warning('Re writing file {}'.format(outfile))
        with open(outfile,'w') as _oh:
            _oh.write('Therapeutic_area_codes\tDisease_ids\n')
            for c in sorted(taMap.keys()):
                for did in sorted(taMap[c]):
                    _oh.write('{}\t{}\n'.format(c,did))
    
    def _getIdChecker(self,filterIds):
        def doNotSkip(json,filterIds):
            return False
        def skipOnAbsent(json,filterIds):
            if json['disease']['id'] not in filterIds:
                return True
            else:
                return False
        if filterIds is None:
            return doNotSkip
        else:
            return skipOnAbsent