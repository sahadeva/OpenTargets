import logging
import time
import os
from lib.variantparser import VariantParser
from lib.disease_variantparser import DiseaseVariantParser
from lib.eparsers import clinVarParser, snpParser
from lib.ensemblparser import EnsemblRequest
from lib.gffparser import GffParser
from lib.intervalchecker import ChromIntervals
from lib.exceptions import NoVariantMappingError

class VariantAnnotator:
    
    def __init__(self,otDump,minScore,email,retmax,clinvarMeasureSet,clinvarAssembly):
        self.otDump = otDump
        self.minScore = minScore
        self.email = email
        self.retmax = retmax
        self.clinvarMeasureSet = clinvarMeasureSet
        self.clinvarAssembly = clinvarAssembly
        # init needed dictionaries
        self._geneNameMap = {}
        self._diseaseNameMap = {}
        self._diseaseGeneVar = {}
        self._diseaseGeneAssoc = {}
        self._varLocMap = {}
        self._geneLocMap = {}
        self._geneVarMap = {}
    
    def _sanityCheck(self,dat,name):
        '''
        helper function
        check if given dat is empty and throw an exception
        '''
        if len(dat)==0:
            raise ValueError('Cannot parse {} from {}. Check your dataset and input parameters!'.format(name,self.otDump))
    
    def _getGeneLocations(self,gff):
        '''
        helper function
        get gene location data
        '''
        with GffParser(gff=gff) as _gf:
            _gf.parseGff()
            self._geneLocMap = _gf.getGenePosition()
        missingGenes =  set(self._geneNameMap.keys()) - set(self._geneLocMap.keys())
        if len(missingGenes)>0:
            logging.info('Cannot find chromosomal locations for {} genes, fetching data from ENSEMBL'.format(len(missingGenes)))
            ensparser = EnsemblRequest(missingGenes)
            for ids, dat in ensparser.getPosition().items():
                self._geneLocMap[ids] = dat
        else:
            logging.info('Found positional data for {} genes from file {}'.format(len(self._geneNameMap),gff))
    
    def _getDbSnpLoc(self,dbSnpIds):
        '''
        helper function
        populate self._varLocMap with chromosomal location data from dbsnp database
        '''
        if len(dbSnpIds)==0:
            logging.warning('Cannot find dbSNP ids! Check your dataset and input parameters')
        else:
            time.sleep(10)
            # wait for 10 sec before fetching clinvar data
            # to not to overload Entrez servers
            snpparser = snpParser(dbSnpIds,self.email,self.retmax)
            snpparser.efetch()
            self._varLocMap.update(snpparser.mapping)
    
    def _getClinvarLoc(self,clinvarIds):
        '''
        helper function
        populate self._varLocMap with chromosomal location data from clinvar database
        '''
        if len(clinvarIds)==0:
            logging.warning('Cannot find clinVar ids! Check your dataset and input parameters')
        else:
            time.sleep(10)
            # wait for 10 sec before fetching clinvar data
            # to not to overload Entrez servers
            clinparser = clinVarParser(clinvarIds,self.email,self.retmax,self.clinvarMeasureSet,self.clinvarAssembly)
            clinparser.efetch()
            self._varLocMap.update(clinparser.mapping)
    
    def _getNoDbSnpLoc(self,noDbSnpIds):
        if len(noDbSnpIds)==0:
            logging.warning('Cannot find no dbSNP data!')
        for nids in noDbSnpIds:
                chrom,pos,reflen = nids.split('_')
                begin = int(pos)-1
                end = begin+int(reflen)
                self._varLocMap[nids] = [[chrom,begin,end,'*']]
    
    def _mapVariantsToGenes(self):
        if self._varLocMap == 0:
            raise NoVariantMappingError('Cannot find variant id to position mapping data! Check your data')
        ci = ChromIntervals(self._geneLocMap)
        self._geneVarMap = ci.getGeneVariantOverlaps(self._varLocMap)
    
    def _writeData(self,out):
        with open(out,'w') as _oh:
            for disease in self._diseaseGeneVar:
                commonGenes = set(self._diseaseGeneVar[disease].keys()) & set(self._geneVarMap.keys())
                if len(commonGenes)==0:
                        logging.warning('Cannot find any genes with chromosome position for {}, skipping...'.format(disease))
                        continue
                diseaseName = self._diseaseNameMap[disease] if disease in self._diseaseNameMap else 'unknown'
                for gene in commonGenes:
                    geneLoc = self._geneLocMap[gene]
                    geneName = self._geneNameMap[gene] if gene in self._geneNameMap else 'undefined'
                    commonIds = set(self._diseaseGeneVar[disease][gene].keys()) & set(self._geneVarMap[gene].keys())
                    if len(commonIds)==0:
                        logging.warning('Cannot find chromosome location of variants in disease {}, gene {}, skipping'.format(disease,gene))
                        continue
                    for ids in commonIds:
                        varLoc = self._geneVarMap[gene][ids]
                        assoScore = self._diseaseGeneVar[disease][gene][ids]
                        try:
                            assoSet = ','.join(self._diseaseGeneAssoc[disease][gene][ids])
                        except KeyError:
                            assoSet = 'None'
                        varName = '|'.join([ids,gene,geneName,disease,diseaseName,assoSet])
                        bedString = '{0}\t{1}\t{2}\t{3}\t{4}\t{5}\n'.format(geneLoc[0],varLoc[0],varLoc[1],varName,assoScore,geneLoc[-1])
                        _oh.write(bedString)

    def getVariants(self,assoTypes,out,gff,databases=None):
        logging.info('Parsing {} for {} variants, minimum association score required: {}'.format(self.otDump,','.join(assoTypes),self.minScore))
        with VariantParser(otDump=self.otDump,assocTypes=assoTypes,minScore=self.minScore,databases=databases) as vp:
            vp.parser()
            self._geneNameMap = vp.geneIdName
            self._diseaseNameMap = vp.diseaseIdName
            self._diseaseGeneVar = vp.diseaseGeneVariance
            self._diseaseGeneAssoc = vp.diseaseGeneAssociation
            self._getClinvarLoc(vp.clinvarIds)
            self._getDbSnpLoc(vp.dbSnpIds)
            self._getNoDbSnpLoc(vp.noDbSnpIds)
        self._sanityCheck(self._geneNameMap,'gene id to name mappings')
        self._sanityCheck(self._diseaseNameMap,'disease id to name mappings')
        self._sanityCheck(self._diseaseGeneVar,'disease gene variance data')
        self._getGeneLocations(gff=gff)
        self._mapVariantsToGenes()
        if os.path.exists(out):
            logging.warning('Re writing file {}'.format(out))
        self._writeData(out)
    
    def getDiseaseVariants(self,diseaseIds,gff,out):
        logging.info('Parsing {} for disease variants (disease ids found:{}), minimum association score required: {}'.format(self.otDump,len(diseaseIds),self.minScore))
        with DiseaseVariantParser(otDump=self.otDump,filterIds=diseaseIds,minScore=self.minScore) as dvp:
            dvp.parser()
            self._geneNameMap = dvp.geneIdName
            self._diseaseNameMap = dvp.diseaseIdName
            self._diseaseGeneVar = dvp.diseaseGeneVariance
            self._diseaseGeneAssoc = dvp.diseaseGeneAssociation
            self._getClinvarLoc(dvp.clinvarIds)
            self._getDbSnpLoc(dvp.dbSnpIds)
            self._getNoDbSnpLoc(dvp.noDbSnpIds)
        self._sanityCheck(self._geneNameMap,'gene id to name mappings')
        self._sanityCheck(self._diseaseNameMap,'disease id to name mappings')
        self._sanityCheck(self._diseaseGeneVar,'disease gene variance data')
        self._getGeneLocations(gff=gff)
        self._mapVariantsToGenes()
        if os.path.exists(out):
            logging.warning('Re writing file {}'.format(out))
        self._writeData(out)
