import logging
import os
import networkx
import obonet
import re

from .exceptions import OboUndirectedNetworkError,NoDecendantsError


class EfoParser:

    def __init__(self,obo):
        self.obo = obo
        self._efo = obonet.read_obo(self.obo)
        self._idNameMap = {}
        self._sanityCheck()
        self._removeEdges()
    
    def _sanityCheck(self):
        logging.info('EFO Ontology found {} terms and {} relations in {}'.format(self._efo.number_of_nodes(),self._efo.number_of_edges(),self.obo))
        directed = networkx.is_directed(self._efo)
        if directed:
            logging.info('Found directed network')
        else:
            logging.error('Network parsed from {} is undirected. Cannot work with undirected data!'.format(slef.obo))
            raise OboUndirectedNetworkError('{} creates undirected network. Check your input file'.format(self.obo))
        #self._idNameMap = {id_: data.get('name') for id_, data in self._efo.nodes(data=True)}
        for id_, data in self._efo.nodes(data=True):
            self._idNameMap[id_.lower()] = id_
            if 'name' in data:
                self._idNameMap[data['name'].lower()] = id_
    
    def _removeEdges(self):
        '''
        remove all edges except is_a and part_of
        '''
        removeEdgeList = list()
        keepEdges = set(['is_a','part_of'])
        for e in self._efo.edges:
            if e[2] not in keepEdges:
                removeEdgeList.append([e[0],e[1]])
        logging.info('Removing {} relations that are not "is_a" or "part_of"'.format(len(removeEdgeList)))
        for r in removeEdgeList:
            self._efo.remove_edge(r[0],r[1])

    def getDescendants(self,ids):
        checkId = re.match(r'^[A-Z]+(\|\:)\d+$',ids,re.IGNORECASE)
        ids = ids.lower()
        if checkId:
            if checkId.group1=='_':
                ids = ids.replace('_',':')
        if ids in self._idNameMap:
            realId = self._idNameMap[ids]
            descNodes =  set(subterm for subterm in networkx.ancestors(self._efo, realId))
            if len(descNodes)==0:
                raise NoDecendantsError('Cannot find any child nodes of {} in {}'.format(ids,self.obo))
            else:
                logging.info('Found {} child nodes for id/term "{}"'.format(len(descNodes),ids))
                return descNodes
        else:
            raise LookupError('Cannot find id/term {} in {} ontology!'.format(ids,self.obo))
    
    def mapIdToName(self,out):
        if os.path.exists(out):
            logging.warning('Re writing file {}'.format(out))
        with open(out,'w') as _oh:
            _oh.write('EFO_id\tEFO_name\n')
            for ids, data in self._efo.nodes(data=True):
                if 'name' in data:
                    name = data['name']
                else:
                    name = ids
                _oh.write('{}\t{}\n'.format(ids.replace(':','_'),name))






