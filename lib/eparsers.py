import logging
import math
import re
import xml.etree.ElementTree as ET

from Bio import Entrez

class eParser:
    '''
    given list of ids, parse'em
    '''

    def __init__(self,inputIds,db,rettype,email,retmax=1000):
        self.inputIds = list(inputIds)
        self.retmax = retmax
        self.counter = int(math.ceil(len(self.inputIds)/self.retmax))
        self.db = db
        self.rettype = rettype
        Entrez.email = email
        Entrez.max_tries = 5
        #self.idList = list(self.inputIds)
        self._mapping = {}
    
    def efetch(self):
        # @TODO: convert this to custom chunking to reduce request size
        
        if self.counter==1:
            ids = ','.join(self.inputIds)
            logging.debug('Ids: {}'.format(ids))
            eResp = Entrez.efetch(db=self.db, id=ids, rettype=self.rettype,retmax=self.retmax)
            eElemTree = ET.parse(eResp)
            logging.debug(eResp.url)
            logging.info('db: {}, ids {} of {}'.format(self.db,len(self.inputIds),len(self.inputIds)))
            self._responseParser(eElemTree)
        else:
            retstart = 0
            retmax = self.retmax
            for i in range(self.counter):
                logging.info('db: {}, chunk {}, ids {} of {}'.format(self.db,i+1,retmax,len(self.inputIds)))
                ids=','.join(self.inputIds[retstart:retmax])
                logging.debug('Ids: {}'.format(ids))
                logging.debug('from {} to {}'.format(retstart,retmax))
                eResp = Entrez.efetch(db=self.db, id=ids, rettype=self.rettype,retmax=self.retmax)
                eElemTree = ET.parse(eResp)
                logging.debug(eResp.url)
                retstart = retmax
                retmax = min(retmax+self.retmax,len(self.inputIds))
                self._responseParser(eElemTree)

    def _namespace(self,element):
        '''
        return xml namespace
        source: https://stackoverflow.com/questions/9513540/python-elementtree-get-the-namespace-string-of-an-element
        '''
        m = re.match(r'\{.*\}', element.tag)
        return m.group(0) if m else ''

    def _responseParser(self,eElemTree,**kwargs):
        pass

    @property
    def mapping(self):
        return self._mapping

class snpParser(eParser):

    '''
    Given a list of input rsids, return chromosome positions
    '''

    def __init__(self,inputIds,email,retmax=1000):
        '''
        database: snp
        rettype: xml
        '''
        super().__init__(inputIds,'snp','xml',email,retmax)
    
    def _responseParser(self, eElemTree):
        dbNS = self._namespace(eElemTree.getroot())
        for ids,loc in zip(eElemTree.findall('.//{}SNP_ID'.format(dbNS)),eElemTree.findall('.//{}CHRPOS'.format(dbNS))):
            rsId = ids.text
            locText = loc.text
            if rsId is None or locText is None:
                logging.warning('SNP id: {} location: {}! skipping....'.format(rsId,locText))
                continue
            rsId = 'rs'+rsId # 'rs' is not in snp id
            chrom,pos = locText.split(':')
            if chrom == 'MT':
                chrom = 'M'
            self._mapping[rsId] = [['chr'+chrom,int(pos)-1,int(pos),'*']] # chromosome, start, stop, strand in BED format

class clinVarParser(eParser):
    
    def __init__(self, inputIds, email, retmax=1000, measureSet='Variant', assembly='GRCh38'):
        '''
        database: clinvar
        rettype: clinvarset
        '''
        self.measureSet = '"'+measureSet+'"'
        self.assembly = '"'+assembly+'"'
        super().__init__(inputIds, 'clinvar', 'clinvarset', email, retmax=retmax)
        
    def _responseParser(self, eElemTree):
        for evar in eElemTree.getroot():
            ids = evar.find('.//ReferenceClinVarAssertion/ClinVarAccession').get('Acc')
            if ids is None:
                logging.warning('None found instead of valid id! skipping...')
                continue
            for sl in evar.findall('.//ReferenceClinVarAssertion/MeasureSet[@Type={0}]/Measure/SequenceLocation[@Assembly={1}]'.format(self.measureSet,self.assembly)):
                chrom = sl.get('Chr')
                start = sl.get('start')
                stop = sl.get('stop')
                strand = sl.get('Strand')
                if strand is None:
                    strand = '*'
                elif (chrom is None) or (start is None) or (stop is None):
                    logging.warning('{} chromosome location: {}:{}-{}! skipping...'.format(ids,chrom,start,stop))
                    continue
                if chrom == 'MT':
                    chrom = 'M'
                chrom = 'chr'+chrom
                start = int(start)-1
                stop = int(stop)
                try:
                    self._mapping[ids].append([chrom,start,stop,strand])
                except KeyError:
                    self._mapping[ids] = [[chrom,start,stop,strand]]
