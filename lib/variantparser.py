import gzip
import json
import re
import logging

from collections import defaultdict

from lib.otparser import OpenTargetsParser

class VariantParser(OpenTargetsParser):

    def __init__(self, otDump, assocTypes,databases=None,minScore=0.2):
        '''
        source: https://stackoverflow.com/questions/19189274/nested-defaultdict-of-defaultdict
        '''
        self.assocTypes = assocTypes
        self._diseaseGeneVar = defaultdict(lambda: defaultdict(dict))
        self._diseaseGeneAssoc = defaultdict(lambda: defaultdict(dict))
        self._clinvarIds = set()
        self._dbSnpIds = set()
        self._noDbSnpIds = set()
        if databases is None:
            self.databases = None
            logging.debug('No databases to filter')
        else:
            self.databases = set(databases)
            logging.debug('Filter results for {}'.format(', '.join(self.databases)))
        self._skippedMap = {}
        self._parsedMap = {}
        self._nucRe = re.compile(r'^[A|T|G|C|N]{1,}$',re.IGNORECASE)
        super().__init__(otDump=otDump, minScore=minScore)    
    
    def _dbChecker(self):
        
        def _noDBcheck(jsonString):
            return False
        
        def _DBcheck(jsonString):
            # ['evidence']['variant2disease']['provenance_type']['database']['id']
            # or ['evidence']['provenance_type']['database']['id']
            try:
                dbIds = set([jsonString['evidence']['variant2disease']['provenance_type']['database']['id']])
            except KeyError:
                try:
                    dbIds = set([jsonString['evidence']['provenance_type']['database']['id']])
                except KeyError:
                    return True
            commonDB = dbIds&self.databases
            if len(commonDB)==0:
                for di in dbIds:
                    try:
                        self._skippedMap[di]+=1
                    except KeyError:
                        self._skippedMap[di]=1
                return True
            else:
                for di in commonDB:
                    try:
                        self._parsedMap[di]+=1
                    except KeyError:
                        self._parsedMap[di]=1
                return False

        if self.databases is None:
            logging.debug('No DB check')
            return _noDBcheck
        else:
            logging.debug('Return DB checker')
            return _DBcheck

    def parser(self):
        i=0
        ai = 0
        rej = 0
        skipEntry = self._dbChecker()
        for ol in self._otH:
            otJson = json.loads(ol)
            i+=1
            varId = None
            skip = False
            if (i%100000)==0:
                logging.info('Parsed {} lines'.format(i))
            if (otJson['type'] not in self.assocTypes) or  (otJson['scores']['association_score']<= self.minScore) or (skipEntry(otJson)):
                # type and min association score skipper
                continue
            elif ('unique_association_fields' not in otJson) and ('variant' not in otJson):
                continue
            elif 'unique_association_fields' in otJson:
                # @TODO : rewrite this!
                if ('variant_id' in otJson['unique_association_fields']) and ('clinvarAccession' in otJson['unique_association_fields']):
                    if otJson['unique_association_fields']['variant_id'].startswith('rs'):
                        varId = otJson['unique_association_fields']['variant_id']
                        self._dbSnpIds.add(varId)
                    else: 
                        varId = otJson['unique_association_fields']['clinvarAccession']
                        self._clinvarIds.add(varId)
                elif ('variant_id' in otJson['unique_association_fields']) and ('dbSnps' in otJson['unique_association_fields']):
                    varId = otJson['unique_association_fields']['dbSnps']
                    self._dbSnpIds.add(varId)
                elif 'dbSnps' in otJson['unique_association_fields']:
                    varId = otJson['unique_association_fields']['dbSnps']
                    self._dbSnpIds.add(varId)
                elif 'variant_id' in otJson['unique_association_fields']:
                    if otJson['unique_association_fields']['variant_id'].startswith('rs'):
                        varId = otJson['unique_association_fields']['variant_id']
                        self._dbSnpIds.add(varId)
                    elif otJson['unique_association_fields']['variant_id'].startswith('RCV'):
                        varId = otJson['unique_association_fields']['variant_id']
                        self._clinvarIds.add(varId)
                    else:
                        logging.info('{} id with unknown source! skipping'.format(otJson['unique_association_fields']['variant_id']))
                        skip = True
                else:
                    skip = True
            if 'variant' in otJson and varId is None:
                # @TODO: rewrite this!
                skip = True
                if 'rs_id' in otJson['variant']:
                    varId  = otJson['variant']['rs_id']
                    self._dbSnpIds.add(varId)
                    skip = False
                elif 'id'  in otJson['variant']:
                    tmpId = otJson['variant']['id'].replace('//','/').split('/')
                    if len(tmpId)>3:
                        if tmpId[2].lower()=='dbsnp':
                            self._dbSnpIds.add(tmpId[-1])
                        elif tmpId[2].lower()=='clinvar':
                            self._clinvarIds.add(tmpId[-1])
                        varId = tmpId[-1]
                        skip = False
                    else:
                        tmpId = otJson['variant']['id'].split('_')
                        if len(tmpId)<4:
                            continue
                        elif re.match(self._nucRe,tmpId[2]) and re.match(self._nucRe,tmpId[3]):
                            if tmpId[0] == 'MT':
                                tmpId[0] = 'chrM'
                            elif not tmpId[0].startswith('chr'):
                                tmpId[0] = 'chr'+tmpId[0] 
                            varId = '_'.join([tmpId[0],tmpId[1],str(len(tmpId[2]))])
                            self._noDbSnpIds.add(varId)
                            skip = False
                else:
                    skip = True
            if skip:
                continue
            try:
                geneId = otJson['target']['id']
            except KeyError:
                # no gene id
                logging.warning('Skipping current line, cannot parse gene id')
                continue
            try:
                diseaseId = otJson['disease']['id']
            except KeyError:
                # no disease id
                logging.warning('Skipping current line, cannot parse disease id')
                continue
            ai+=1
            self._diseaseGeneVar[diseaseId][geneId][varId] = otJson['scores']['association_score']
            # gene id to name mapping
            self._setGeneIdName(otJson['target'])
            # disease id to name mapping
            self._setDiseaseIdName(otJson['disease'])
            try:
                self._diseaseGeneAssoc[diseaseId][geneId][varId].add(otJson['type'])
            except KeyError:
                self._diseaseGeneAssoc[diseaseId][geneId][varId] = set([otJson['type']])
        logging.info('Parsed {} lines'.format(i))
        logging.debug('Skipped {} entries'.format(rej))
        logging.info('Number of {}(s) found {}'.format(', '.join(self.assocTypes),ai))
        if self.databases is not None:
            for db,c in self._parsedMap.items():
                logging.info('Database: {} found associations: {}'.format(db,c))
            for db,c in self._skippedMap.items():
                logging.info('Database: {} skipped associations: {}'.format(db,c))
        logging.info('{} genes mapped to {} diseases'.format(len(self._geneId2Name),len(self._diseaseId2Name)))
    
    @property
    def clinvarIds(self):
        return self._clinvarIds
    
    @property
    def dbSnpIds(self):
        return self._dbSnpIds
    
    @property
    def noDbSnpIds(self):
        return self._noDbSnpIds
    
    @property
    def diseaseGeneVariance(self):
        return self._diseaseGeneVar
    
    @property
    def diseaseGeneAssociation(self):
        return self._diseaseGeneAssoc