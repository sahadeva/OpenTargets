import logging
from collections import defaultdict
from intervaltree import Interval, IntervalTree

class ChromIntervals:
    '''
    Generate interval per chromosome using gene intervals
    '''
    def __init__(self,geneLocMap):
        self._intervals = {}
        self._createIntervals(geneLocMap)
    
    def _createIntervals(self,geneLocMap):
        gc = 0
        for gi,dat in geneLocMap.items():
            try:
                self._intervals[dat[0]].addi(dat[1],dat[2],gi)
            except KeyError:
                self._intervals[dat[0]] = IntervalTree()
                self._intervals[dat[0]].addi(dat[1],dat[2],gi)
            gc+=1
        logging.info('Chromosomes: {}, genes: {}'.format(len(self._intervals),gc))
    
    def getGeneVariantOverlaps(self,varLocMap):
        geneVarMap = defaultdict(dict)
        for ids,loc in varLocMap.items():
            # varLocMap can have multiple positions for a single variant
            for iloc in loc:
                if iloc[0] not in self._intervals:
                    logging.warning('Cannot find {} in gene locations!')
                    continue
                for gloc in self._intervals[iloc[0]].overlap(iloc[1],iloc[2]):
                    if gloc.begin>iloc[1] or gloc.end<iloc[2]:
                        # only use variants completely within a gene
                        continue
                    geneVarMap[gloc.data][ids] = [iloc[1],iloc[2]]
        return geneVarMap


