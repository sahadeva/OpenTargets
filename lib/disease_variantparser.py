import gzip
import json
import re
import logging

from collections import defaultdict

from lib.otparser import OpenTargetsParser

class DiseaseVariantParser(OpenTargetsParser):
    '''
    Parse all disease related variants irrespective of association types
    '''
    def __init__(self, otDump, filterIds, minScore=0.2):
        super().__init__(otDump, minScore=minScore)
        self.filterIds = filterIds
        self._diseaseGeneVar = defaultdict(lambda: defaultdict(dict))
        self._diseaseGeneAssoc = defaultdict(lambda: defaultdict(dict))
        self._clinvarIds = set()
        self._dbSnpIds = set()
        self._noDbSnpIds = set()
        self._nucRe = re.compile(r'^[A|T|G|C|N]{1,}$',re.IGNORECASE)
    
    def parser(self):
        i = 0
        for ol in self._otH:
            varIds = None
            otJson = json.loads(ol)
            i+=1
            if (i%200000)==0:
                logging.info('Parsed {} lines'.format(i))
            if (otJson['scores']['association_score']<= self.minScore) or ('disease' not in otJson):
                # type and min association score skipper
                continue
            try:
                diseaseId = otJson['disease']['id']
            except KeyError:
                # no disease id
                logging.warning('Skipping current line, cannot parse disease id')
                continue
            try:
                geneId = otJson['target']['id']
            except KeyError:
                # no gene id
                logging.warning('Skipping current line, cannot parse gene id')
                continue
            if otJson['disease']['id'] not in self.filterIds:
                continue
            elif 'unique_association_fields' in otJson:
                varIds = self._unique_association_fields(otJson)
            if 'variant' in otJson and varIds is None:
                varIds = self._variant(otJson)
            if varIds is None:
                continue
            # gene id to name mapping
            self._setGeneIdName(otJson['target'])
            # disease id to name mapping
            self._setDiseaseIdName(otJson['disease'])
            for varId in varIds:
                self._diseaseGeneVar[diseaseId][geneId][varId] = otJson['scores']['association_score']
                try:
                    self._diseaseGeneAssoc[diseaseId][geneId][varId].add(otJson['type'])
                except KeyError:
                    self._diseaseGeneAssoc[diseaseId][geneId][varId] = set([otJson['type']])
        logging.info('{} genes mapped to {} diseases'.format(len(self._geneId2Name),len(self._diseaseId2Name)))
            
    def _unique_association_fields(self,otJson):
        '''
        Helper function, return variant ids from unique association fields
        '''
        variantId = None
        rsId = None
        clinvarId = None
        variantId = otJson['unique_association_fields']['variant_id'].split(', ') if 'variant_id' in otJson['unique_association_fields'] else None
        rsId = otJson['unique_association_fields']['dbSnps'].split(', ') if 'dbSnps' in otJson['unique_association_fields'] else None
        clinvarId = otJson['unique_association_fields']['clinvarAccession'].split(', ') if 'clinvarAccession' in otJson['unique_association_fields'] else None 
        # logging.debug(' variant id {}, dbsnp id {}, clinvar id {}'.format(variantId,rsId,clinvarId))
        if (variantId is not None) and (clinvarId is not None):
            if variantId[0].startswith('rs'):
                self._dbSnpIds.update(variantId)
                clinvarId = None
        if rsId is not None:
            self._dbSnpIds.update(rsId)
            return rsId
        if clinvarId is not None:
            self._clinvarIds.update(clinvarId)
            return clinvarId
        if variantId is not None:
            return variantId
        else:
            return None

    def _variant(self,otJson):
        '''
        Helper function, return vairant ids from variant tags
        '''
        if 'rs_id' in otJson['variant']:
            rsId = otJson['variant']['rs_id'].split(', ')
            self._dbSnpIds.update(rsId)
            return rsId
        elif 'id' in otJson['variant']:
            tmpId = otJson['variant']['id'].replace('//','/').split('/')
            if len(tmpId)>=3:
                if tmpId[2].lower()=='dbsnp':
                    pass
                    self._dbSnpIds.add(tmpId[-1])
                elif tmpId[2].lower()=='clinvar':
                    pass
                    self._clinvarIds.add(tmpId[-1])
                return [tmpId[-1]]
            else:
                tmpId = otJson['variant']['id'].split('_')
                if len(tmpId)<4:
                    return None
                if re.match(self._nucRe,tmpId[2]) and re.match(self._nucRe,tmpId[3]):
                    if tmpId[0] == 'MT':
                        tmpId[0] = 'chrM'
                    elif not tmpId[0].startswith('chr'):
                        tmpId[0] = 'chr'+tmpId[0] 
                    varId = '_'.join([tmpId[0],tmpId[1],str(len(tmpId[2]))])
                    self._noDbSnpIds.add(varId)
                    return [varId]
                else:
                    return None
    
    @property
    def clinvarIds(self):
        return self._clinvarIds
    
    @property
    def dbSnpIds(self):
        return self._dbSnpIds
    
    @property
    def noDbSnpIds(self):
        return self._noDbSnpIds
    
    @property
    def diseaseGeneVariance(self):
        return self._diseaseGeneVar
    
    @property
    def diseaseGeneAssociation(self):
        return self._diseaseGeneAssoc
            

