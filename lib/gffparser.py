import logging
import gzip
import re
from collections import OrderedDict

class GffParser:
    '''
    GFF/GTF reader and data converter
    '''

    def __init__(self, gff, gene_id='gene_id', removeVersion = True):
        self.gff = gff
        self.gene_id = gene_id
        self.removeVersion = True
        self._geneDat = {} # [gene_id] = Gene()
    
    def __enter__(self):
        if self._is_gzip():
            self._gh = gzip.open(self.gff,'rt')
        else:
            self._gh = open(self.gff,'r')
        self._xrefParser = self._get_xref_parser()
        return self
    
    def __exit__(self,exceptType,exceptVal,exceptTback):
        self._gh.close()
        if exceptType:
            logging.exception(exceptVal)

    def _is_gzip(self):
        if re.match(r'.*\.gz$',self.gff,re.IGNORECASE):
            return True
        else:
            return False
    
    def _get_xref_parser(self):
        '''
        Return parser for gff/gtf column 9 (attribute column)
        '''
        gtf_pat = re.compile(r'(\w+)\s{1,}"([^"]+)"', re.IGNORECASE)
        gff_pat  = re.compile(r'(\w+)\=([^;]+)',re.IGNORECASE)
        
        def gtf_xref(featstr):
            '''
            annoying gtf attrib format!
            '''
            xlist = re.findall(gtf_pat, featstr)
            xdict = OrderedDict()
            for xi in xlist:
                try:
                    xdict[xi[0]] = ",".join([xdict[xi[0]],xi[1]])
                except KeyError:
                    xdict[xi[0]] = xi[1]
            return xdict
        
        def gff_xref(featstr):
            '''
            All this gimmick to adjust for the annoying gtf attrib format!
            '''
            xlist = re.findall(gff_pat, featstr)
            xdict = OrderedDict()
            for xi in xlist:
                xdict[xi[0]] = xi[1]
            return xdict
        
        if re.match('^.*gtf.*', self.gff, re.IGNORECASE):
            logging.info('Detected file format: GTF')
            return gtf_xref
        elif re.match('.*gff.*', self.gff, re.IGNORECASE):
            logging.info('Detected file format: GFF')
            return gff_xref
        else:
            raise NotImplementedError('Cannot recognize {} file format from suffix'.format(self.gff))
    
    def parseGff(self):
        '''
        parse the given GFF file to data structure
        '''
        i = 0
        for f in self._gh:
            if type(f)==bytes:
                f = f.decode(encoding='utf-8')
            if f[0]=='#':
                continue
            fDat = f.strip().split('\t')
            if len(fDat)<9:
                logging.warning('Skipping {}, not enough columns!'.format(f.strip('\n')))
                continue
            i+=1
            if (i%100000)==0:
                logging.info('Lines parsed: {}'.format(i))
            if fDat[2].lower() != 'gene' and fDat[-1][:7]!='ID=gene':
                continue
            xrefs = self._xrefParser(fDat[-1])
            if self.gene_id not in xrefs:
                logging.warning('Cannot find {}  in {}'.format(self.gene_id,f.strip('\n')))
                continue
            if fDat[0] == 'MT':
                fDat[0] = 'chrM'
            elif not fDat[0].startswith('chr'):
                fDat[0] = 'chr'+fDat[0]
            begin = int(fDat[3])-1
            end = int(fDat[4])
            geneId = xrefs[self.gene_id].split('.')[0] if self.removeVersion else xrefs[self.gene_id]
            self._geneDat[geneId] = [ fDat[0],begin,end,fDat[6] ]
        logging.info('Lines parsed: {}'.format(i))
        if len(self._geneDat)==0:
            raise ValueError('Cannot parse "Gene" and related features from {}. Check your input!'.format(self.gff))
        logging.info('Parsed {} "Gene" features from {}'.format(len(self._geneDat),self.gff))
    
    def getGenePosition(self):
        return self._geneDat